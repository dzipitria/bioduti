# -*- coding: utf-8 -*-
#   Este programa es software libre y se publica bajo GNU Lesser General Public v3
#
#   Para la realización de este proyecto se usaron las librerias: DEAP, NUMPY, JSON que al igual que
#   Python son software libre GNU
#   Puede ver la licencia GNU en: http://www.gnu.org/licenses/
#   
#   Daniel Zipitría

from random import randint
from random import uniform
import random
import math
from scipy.interpolate import Rbf

def Distancia(a, b):
    """ Funcion de calvulo de distancia euclidiana en Rn"""
    x = 0.0
    for i in range(len(a)):
        x = x + (a[i] - b[i] ) ** 2
    return math.sqrt(x)

# ------------- Funciones de mutación y cruzamiento para listas de reales. ---------------------
def R(a, b):
    return uniform(a,b)

def BLX(x, y, valmax, valmin, alfa=0.5):
    m = 0
    n = 0
    if (x > y):
        m = x
        n = y
    else:
        m = y
        n = x
    d = m - n
    rangmin = n - d * alfa
    rangmax = m + d * alfa
    if rangmax > valmax:
        rangmax = valmax
    if rangmin < valmin:
        rangmin = valmin
    return R(rangmin, rangmax)

# def CruzaBlx1(padre1, padre2, alfa):
#     genes_agregados = set()
#     genes_agregados.add(12)
#     hijo = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
#     for i in range(len(hijo)):
#         aux = randint(0,11)
#         genes_agregados.add(aux)
#         if aux in genes_agregados:
#             for j in range(1,20):
#                 aux = randint(0,11)
#                 if aux not in genes_agregados:
#                     genes_agregados.add(aux)
#                     break
#         if aux > 5:
#             aux = aux - 6
#             hijo[i] = padre2[aux]
#         else:
#             hijo[i] = padre1[aux]
#     z = randint(0,5)
#     hijo[z] = BLX(padre1[z], padre2[z], alfa)
#     return hijo


def CruzaBlx2(padre1, padre2, valmax, valmin, alfa=0.5):
    #  Cruzamiento de listas de Reales generando 2 hijos
    hijo = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    for i in  range(len(hijo)):
        hijo[i] = BLX(padre1[i], padre2[i], valmax[i], valmin[i], alfa)
    return hijo

def Delta(t, g, y, b=1.0):
    r = uniform(0.0, 1.0)
    if b==1.0:  # Para intentar dar un poco más de velocidad al no tener que cargar una función de C que no hace nada
        exponente = 1 - float(t) / float(g)
    else:
        exponente = math.pow(1 - float(t) / float(g), b)
    return y * (1 - math.pow(r, exponente))


def Mut(t, g, xmax, xmin, x, b=1.0):
    z = randint(0, 1)
    if z==0:
        return x + Delta(t, g, xmax - x, b)
    else :
        return x - Delta(t, g, x - xmin, b)

def Muta_bacteria(bacteria, t, g, valmax, valmin,):
#  Cruzamiento de listas de Reales generando 2 hijos
#  Bacteria tiene que estar normalizada no se verifica
#   t = numero de generacion actual del AE
#   g = numero maximo de generaciones del AE
    bac_mutada =  []
    for i in range(len(bacteria)):
        bac_mutada.append(Mut(t, g, valmax[i], valmin[i], bacteria[i], b=1.0))
    return bac_mutada


def estimador_rbf(bact_norm, tiempos, f_dist='multiquadric'):
    # Fue creado como otra forma de ponderar el tiempo para usar en funcion de fitness
    #  Usa el estimador RBFi de scipy para estimar la tiempo de duplicación
    x = [[], [], [], [], [], []]
    for i in range(len(bact_norm)):
        for j in range(len(x)):
            x[j].append(bact_norm[i][j])
    #rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function='thin_plate', epsilon=0.1,smooth=0.0009)
    #rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function='thin_plate')
    #rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function='multiquadric',epsilon=0.006)
    rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function=f_dist)
    #rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function='linear')
    #rbfi = Rbf(x[0], x[1], x[2], x[3], x[4], x[5], tiempos,function='inverse')
    # print rbfi.function, rbfi.smooth, rbfi.epsilon
    return rbfi



if __name__ == "__main__":
    random.seed(None)
    #test mutacion funciona
    for i in range(1000000):
        b=[None]*6
        for i in range(6):
            b[i] = random.random()
        m = Muta_bacteria(b, 1, 100, [1.0,]*6, [0.0,]*6 )
        for i in range(6):
            if m[i] < 0 or m[i] > 1:
                print "Error:", i,"\n", b,"\n", m
    #test cruzamiento funciona
    for i in range(1000000):
        b1=[None]*6
        b2=[None]*6
        for i in range(6):
            b1[i] = random.random()
            b2[i] = random.random()
        h = CruzaBlx2(b1, b2, [1.0,]*6, [0.0,]*6 )
        for i in range(6):
            if h[i] < 0 or h[i] > 1:
                print "Error:", i,"\n", b,"\n", m


    # bacterias, tiempos = Cargo_bacterias()
    # print tiempos[0]
    # print tiempos[1]
    # print tiempos[24]
    # print tiempos[208]
    # print tiempos[209]
    # for i in range(len(bacterias)):
    #     if not Verifico_bacteria(bacterias[i]):
    #         print("Error en bacteria %i: %s" % (i, str(bacterias[i])))
    # bacterias_norm = Norm_bacterias(bacterias)
    # print "----------Prueba Norm----------"
    # calc_tiempo = estimador(bacterias_norm[2:-2], tiempos[2:-2], "multiquadric")
    # print calc_tiempo(bacterias_norm[0][0], bacterias_norm[0][1], bacterias_norm[0][2], bacterias_norm[0][3],
    #                   bacterias_norm[0][4], bacterias_norm[0][5])
    # print calc_tiempo(bacterias_norm[1][0], bacterias_norm[1][1], bacterias_norm[1][2], bacterias_norm[1][3],
    #                   bacterias_norm[1][4], bacterias_norm[1][5])
    # print calc_tiempo(bacterias_norm[24][0], bacterias_norm[24][1], bacterias_norm[24][2], bacterias_norm[24][3],
    #                   bacterias_norm[24][4], bacterias_norm[24][5])
    # print calc_tiempo(bacterias_norm[208][0], bacterias_norm[208][1], bacterias_norm[208][2], bacterias_norm[208][3],
    #                   bacterias_norm[208][4], bacterias_norm[208][5])
    # print calc_tiempo(bacterias_norm[209][0], bacterias_norm[209][1], bacterias_norm[209][2], bacterias_norm[209][3],
    #                   bacterias_norm[209][4], bacterias_norm[209][5])
    #
    #
    # print "----------Prueba Sin Norm Este dio un buen resultado exepto cuando era un punto conocido ----------"
    # calc_tiempo = estimador(bacterias[2:-2], tiempos[2:-2],'inverse')
    # print 1.0 / calc_tiempo(bacterias[0][0], bacterias[0][1], bacterias[0][2], bacterias[0][3],
    #                   bacterias[0][4], bacterias[0][5])
    # print 1.0 / calc_tiempo(bacterias[1][0], bacterias[1][1], bacterias[1][2], bacterias[1][3],
    #                   bacterias[1][4], bacterias[1][5])
    # print 1.0 / calc_tiempo(bacterias[24][0], bacterias[24][1], bacterias[24][2], bacterias[24][3],
    #                   bacterias[24][4], bacterias[24][5])
    # print 1.0 / calc_tiempo(bacterias[208][0], bacterias[208][1], bacterias[208][2], bacterias[208][3],
    #                   bacterias[208][4], bacterias[208][5])
    # print 1.0 / calc_tiempo(bacterias[209][0], bacterias[209][1], bacterias[209][2], bacterias[209][3],
    #                   bacterias[209][4], bacterias[209][5])
    #
    # print "Fitness --------------------"
    # fit = Fitness(bacterias_norm[0], bacterias_norm[2:-2], tiempos[2:-2])
    # print ("Fitness: %f") %(fit[0])
    # fit = Fitness(bacterias_norm[1], bacterias_norm[2:-2], tiempos[2:-2])
    # print ("Fitness: %f") %(fit[0])
    # fit = Fitness(bacterias_norm[24], bacterias_norm[2:-2], tiempos[2:-2])
    # print ("Fitness: %f") %(fit[0])
    # fit = Fitness(bacterias_norm[208], bacterias_norm[2:-2], tiempos[2:-2])
    # print ("Fitness: %f") %(fit[0])
    # fit = Fitness(bacterias_norm[209], bacterias_norm[2:-2], tiempos[2:-2])
    # print ("Fitness: %f") %(fit[0])


    # for i in range(len(bacterias)):
    #     b = DesNorm(bacterias_norm[i], lim)
    #     if bacterias[i] <> b:
    #         print("Error en bacteria %i: %s <> %s" % (i, str(bacterias[i]), b))
    # x1= Mut(1,1000,45.65,0.34,3.76)
    # print "valor mut " + str(x1)
    # x1= Mut(500,1000,45.65,0.34,3.76)
    # print "valor mut " + str(x1)
    # x1= Mut(800,1000,45.65,0.34,3.76)
    # print "valor mut " + str(x1)
    # x1= Mut(999,1000,45.65,0.34,3.76)
    # print "valor mut " + str(x1)
    # x1= Mut(1000,1000,45.65,0.34,3.76)
    # print "valor mut " + str(x1)
    # valmax = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    # valmin = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    # x = CruzaBlx2([1.0,0.2,0.5,0.7,0.6,0.88],[0.5,0.77,0.4,0.12,0.8,0.98], valmax, valmin)
    # print "valor de BLX1 " + str(x)
    # ind = [50.0, 0.5, 800000, 6.0, 0.3, 5250]
    # print ind
    # indNorm = Norm(ind, lim)
    # print indNorm
    # ind = DesNorm(indNorm, lim)
    # print ind
    # ind = [70.0, 0.2, 3000, 10.0, 0.4, 7200]
    # print ind
    # indNorm = Norm(ind, lim)
    # print indNorm
    # ind = DesNorm(indNorm, lim)
    # print ind
