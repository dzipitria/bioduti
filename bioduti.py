# -*- coding: utf-8 -*-

#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

#   Este programa es software libre y se publica bajo GNU Lesser General Public v3
#
#   Para la realización de este proyecto se usaron las librerias: DEAP, NUMPY, JSON que al igual que
#   Python son software libre GNU
#   Puede ver la licencia GNU en: http://www.gnu.org/licenses/
#   
#   Daniel Zipitría


import random
import array
import numpy
from scipy.interpolate import Rbf
import multiprocessing as mp
from collections import deque
from deap import algorithms
from deap import base
from deap import creator
from deap import tools
import funciones as f
from bacterias import bacts as b
import pickle


def migPipe(deme, k, pipein, pipeout, selection, replacement=None):
    """Migration using pipes between initialized processes. It first selects
    *k* individuals from the *deme* and writes them in *pipeout*. Then it
    reads the individuals from *pipein* and replace some individuals in the
    deme. The replacement strategy shall not select twice the same individual.

    :param deme: A list of individuals on which to operate migration.
    :param k: The number of individuals to migrate.
    :param pipein: A :class:`~multiprocessing.Pipe` from which to read
                   immigrants.
    :param pipeout: A :class:`~multiprocessing.Pipe` in which to write
                    emigrants.
    :param selection: The function to use for selecting the emigrants.
    :param replacement: The function to use to select which individuals will
                        be replaced. If :obj:`None` (default) the individuals
                        that leave the population are directly replaced.
    """
    emigrants = selection(deme, k)
    if replacement is None:
        # If no replacement strategy is selected, replace those who migrate
        immigrants = emigrants
    else:
        # Else select those who will be replaced
        immigrants = replacement(deme, k)
    pipeout.send(emigrants)
    buf = pipein.recv()
    for place, immigrant in zip(immigrants, buf):
        indx = deme.index(place)
        deme[indx] = immigrant
#------------- modificación del standard para que el operador de mutación hacepte el parametro de la generación
def varOr(population, toolbox, lambda_, cxpb, mutpb, gen):
    """Part of an evolutionary algorithm applying only the variation part
    (crossover, mutation **or** reproduction). The modified individuals have
    their fitness invalidated. The individuals are cloned so returnedgenero_bacteria
    population is independent of the input population.

    :param population: A list of individuals to vary.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                    operators.
    :param lambda\_: The number of children to produce
    :param cxpb: The probability of mating two individuals.
    :param mutpb: The probability of mutating an individual.
    :returns: The final population
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution

    The variation goes as follow. On each of the *lambda_* iteration, it
    selects one of the three operations; crossover, mutation or reproduction.
    In the case of a crossover, two individuals are selected at random from
    the parental population :math:`P_\mathrm{p}`, those individuals are cloned
    using the :meth:`toolbox.clone` method and then mated using the
    :meth:`toolbox.mate` method. Only the first child is appended to the
    offspring population :math:`P_\mathrm{o}`, the second child is discarded.
    In the case of a mutation, one individual is selected at random from
    :math:`P_\mathrm{p}`, it is cloned and then mutated using using the
    :meth:`toolbox.mutate` method. The resulting mutant is appended to
    :math:`P_\mathrm{o}`. In the case of a reproduction, one individual is
    selected at random from :math:`P_\mathrm{p}`, cloned and appended to
    :math:`P_\mathrm{o}`.

    This variation is named *Or* beceause an offspring will never result from
    both operations crossover and mutation. The sum of both probabilities
    shall be in :math:`[0, 1]`, the reproduction probability is
    1 - *cxpb* - *mutpb*.
    """
    assert (cxpb + mutpb) <= 1.0, ("The sum of the crossover and mutation "
                                   "probabilities must be smaller or equal to 1.0.")
    offspring = []
    for _ in xrange(lambda_):
        op_choice = random.random()
        if op_choice < cxpb:            # Apply crossover
            ind1, ind2 = map(toolbox.clone, random.sample(population, 2))
            ind1, ind2 = toolbox.mate(ind1, ind2)
            del ind1.fitness.values
            offspring.append(ind1)
        elif op_choice < cxpb + mutpb:  # Apply mutation
            ind = toolbox.clone(random.choice(population))
            ind, = toolbox.mutate(ind, gen)
            del ind.fitness.values
            offspring.append(ind)
        else:                           # Apply reproduction
            offspring.append(random.choice(population))

    return offspring

#------------- modificacion del stnadard: eaMuPlusLambda para agregagar el modelo de islas

def eaMuPlusLambda_islas(population, toolbox, mu, lambda_, cxpb, mutpb, ngen, id_isla, sync, cola_resultados_out,
                   stats=None, halloffame=None, verbose=__debug__):
    """This is the :math:`(\mu + \lambda)` evolutionary algorithm.

    :param population: A list of individuals.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                    operators.
    :param mu: The number of individuals to select for the next generation.
    :param lambda\_: The number of children to produce at each generation.
    :param cxpb: The probability that an offspring is produced by crossover.
    :param mutpb: The probability that an offspring is produced by mutation.
    :param ngen: The number of generation.
    :param stats: A :class:`~deap.tools.Statistics` object that is updated
                  inplace, optional.
    :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                       contain the best individuals, optional.
    :param verbose: Whether or not to log the statistics.
    :returns: The final population
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution.

    The algorithm takes in a population and evolves it in place using the
    :func:`varOr` function. It returns the optimized population and a
    :class:`~deap.tools.Logbook` with the statistics of the evolution. The
    logbook will contain the generation number, the number of evalutions for
    each generation and the statistics if a :class:`~deap.tools.Statistics` is
    given as argument. The *cxpb* and *mutpb* arguments are passed to the
    :func:`varOr` function. The pseudocode goes as follow ::

        evaluate(population)
        for g in range(ngen):
            offspring = varOr(population, toolbox, lambda_, cxpb, mutpb)
            evaluate(offspring)
            population = select(population + offspring, mu)

    First, the individuals having an invalid fitness are evaluated. Second,
    the evolutionary loop begins by producing *lambda_* offspring from the
    population, the offspring are generated by the :func:`varOr` function. The
    offspring are then evaluated and the next generation population is
    selected from both the offspring **and** the population. Finally, when
    *ngen* generations are done, the algorithm returns a tuple with the final
    population and a :class:`~deap.tools.Logbook` of the evolution.

    This function expects :meth:`toolbox.mate`, :meth:`toolbox.mutate`,
    :meth:`toolbox.select` and :meth:`toolbox.evaluate` aliases to be
    registered in the toolbox. This algorithm uses the :func:`varOr`
    variation.
    """
    if id_isla == 0:
        print("Gen.\tCentros\t\tPromedio\t\t\tDesviacion\t\t\tMínimo\t\t\t\tMáximo")
        print("-" * 150)
        sync.set()
    else:
        sync.wait()
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats is not None else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    logbook.log_header = False
    if verbose:
        print logbook.stream

    # Begin the generational process
    coef_migr = int(ngen * 5.0 / 100.0)
    if coef_migr == 0:
        coef_migr = int(ngen  / 2.0)
    for gen in range(1, ngen + 1):
        # Vary the population
        offspring = varOr(population, toolbox, lambda_, cxpb, mutpb, gen)
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)
            # print "Tamaño hof:" , len(halloffame)

        # Select the next generation population
        population[:] = toolbox.select(population + offspring, mu)

        # Update the statistics with the new population
        record = stats.compile(population) if stats is not None else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        #migración --------------------
        if coef_migr > 0 and gen > 0 and gen % coef_migr == 0:
            toolbox.migrate(population)
        #hasa aca ------------------
        if verbose:
            print logbook.stream

#--------------------------------------------------------------
def cxfunction(ind1, ind2):
    hijo = f.CruzaBlx2(ind1, ind2, valmax=b.ValNorMax, valmin=b.ValNorMin)
    if hijo[2] > 1:
        print "cruzamiento:",  hijo[2]
        assert (False) ,("Cruzamiento")
    return creator.Individual(hijo), None   # La fincion generiaca de cruzamiento espera 2 hijos como resultado

def mtfunction(ind, t, g):
    mut = f.Muta_bacteria(ind, t, g, valmax=b.ValNorMax, valmin=b.ValNorMin)
    if mut[2] > 1:
        print ind, t
        print "mutacion:",  mut[2]
        assert (False) ,("Mutacion")
    return  creator.Individual(mut),

def aprox(a, b):
    #return abs(a.fitness.values[1] - b.fitness.values[1]) <= 0.1
    # Para aumentar el muestreo cuando el tiempo es menor a 1.5 horas afecta bacterias FitnessDistRbf
    if a.fitness.values[0] < 1.5 and b.fitness.values[0] < 1.5:
        # print "Menor 10", a.fitness.values[0], b.fitness.values[0]
        return abs(a.fitness.values[0] - b.fitness.values[0]) <= 0.01
    else:
        return abs(a.fitness.values[0] - b.fitness.values[0]) <= 0.1

    #return f.Distancia(a, b) <= 0.05

def main(id_isla, cnt_islas, pipein, pipeout, sync, cola_resultados, gen=500, mu=10, lambd=100, cxpb=0.7, mutpb=0.2):
    #import rpdb2
    #rpdb2.start_embedded_debugger("hola")
    NGEN = gen
    MU = mu
    LAMBDA = lambd
    CXPB = cxpb
    MUTPB = mutpb
    #b.genero_bacterias(isla=id_isla, cant_islas=NBR_ISLAS, cant_ind_isla=mu)
    # Creo el algoritmo evolutivo y sus operaciones
    toolbox = base.Toolbox()
    toolbox.register("genero_bacteria", b.genero_bacteria, isla=id_isla, cant_islas=NBR_ISLAS, cant_ind_isla=mu)
    # toolbox.register("evaluate", b.FitnessDist)
    #toolbox.register("evaluate", b.FitnessRbf)
    toolbox.register("evaluate", b.FitnessDistRbf)
    toolbox.register("mate", cxfunction)
    toolbox.register("mutate", mtfunction, g=NGEN)
    toolbox.register("select", tools.selNSGA2)
    # toolbox.register("select", tools.selSPEA2)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.genero_bacteria)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    # toolbox.register("migrate", migPipe, k=5, pipein=pipein, pipeout=pipeout,
    #     selection=tools.selBest, replacement=random.sample)  # Registro operación de
    toolbox.register("migrate", migPipe, k=1, pipein=pipein, pipeout=pipeout,
        selection=tools.selBest, replacement=random.sample)  # Registro operación de
    pop = toolbox.population(n=MU)
    hof = tools.ParetoFront(aprox)
    #hof = tools.ParetoFront()
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean, axis=0)
    stats.register("std", numpy.std, axis=0)
    stats.register("min", numpy.min, axis=0)
    stats.register("max", numpy.max, axis=0)

    eaMuPlusLambda_islas(pop, toolbox, MU, LAMBDA, CXPB, MUTPB, NGEN,
                   id_isla, sync, cola_resultados_out, stats, halloffame=hof)
    resultados = []
    for h in hof:
        resultados.append(h)
    cola_resultados.send(resultados)



if __name__ == "__main__":

    # if len(sys.argv) != 3:
    #     sys.stderr.write("Syntaxis:\n\t%s <arch_matriz_costos> <arch_matriz_temporadas>" % sys.argv[0])
    #     sys.exit(2)
    # mat_cost = sys.argv[1]
    # mat_temp = sys.argv[2]
    # # Abre los parametros para procesar
    # viaje = viaje_james_bo(mat_cost, mat_temp)  # Rompe un poco el aislamiento de las clases
    # # Crea las colas de combinación con las islas

    # Variables  generales del problema
    NBR_ISLAS = 4  # Cantidad de islas
    tam_pop_isla = 10
    cant_gen = 20
    prob_cx = 0.7
    prob_m = 0.3
    # Fijo una semilla para reproducir el problema None para que sea aleatoria
    semilla = None
    #semilla = 64
    random.seed(semilla) # para que elija siempre los mismos elementos en la mochila
    # Defino las funciones de optimización
    creator.create("FitnessMo", base.Fitness, weights=(-1.0, 1.0))
    creator.create("Individual", array.array, typecode='f', fitness=creator.FitnessMo)
    # Defino el diagrama de islas
    pipes = [mp.Pipe(False) for _ in range(NBR_ISLAS)]
    pipes_in = deque(p[0] for p in pipes)
    pipes_out = deque(p[1] for p in pipes)
    pipes_in.rotate(1)  # Rota la cola de entrada la derecha una posicion
    pipes_out.rotate(-1)  # Rota la cola de salida a la izquierda una posicion
    e = mp.Event()  # Evento de sincronizacion
    cola_resultados_in, cola_resultados_out = mp.Pipe(False)
    #cola_resultados = mp.Queue()
    islas = []
    for i, (ipipe, opipe) in enumerate(zip(pipes_in, pipes_out)):
        parametros = (i, NBR_ISLAS, ipipe, opipe, e, cola_resultados_out, cant_gen, tam_pop_isla, 100, prob_cx, prob_m)
        islas.append(mp.Process(target=main, args=parametros))
    processes = []
    for proc in islas:
        proc.start()
    resultados = []
    for i in range(NBR_ISLAS):
        resultados.append(cola_resultados_in.recv())
    for proc in islas:
        proc.join()
    hof = tools.ParetoFront()
    for result in resultados:
        for r in result:
            # print ("%s, %s") % (str(r), str(r.fitness.values))
            print str(r.fitness.values)
        hof.update(result)
    cp = dict(population=hof)
    with open("checkpoint_name.pkl", "w") as f:
        pickle.dump(cp, f)
    print("================================ Mejores  Resultados ================================")
    for h in hof:
        print str(h.fitness.values)
    print("================================  Desnormalizado  ================================")
    for i in range(len(hof)):
        bd = b.DesNorm(hof[i])
        print "Fitness, Tamcromosoma: %f, %i Bacteria: %s" % (hof[i].fitness.values[0], bd[2], str(bd))
    print("================================  Solo pareto  ================================")
    for i in range(len(hof)):
        bd = b.DesNorm(hof[i])
        print "%f %i " % (hof[i].fitness.values[0], bd[2])
    print len(hof)
