# Boiduti

Biological duplication time.

This project uses the python DEAP library that allows the implementation of Multi Objective Evolutionary Algorithms.

This code is an example of implementation based on NSGA-II selection, on a μ + λ algorithm and an island model that takes advantage of multiprocessing by assigning a computer core to each island to calculate the crossings, mutations and calculation of the fitness function

This code was written in python 2.7 and is intended to be executed with the pypy interpreter (5.3.1) since it has many iterations, the use of JIT compiler generates an improvement in the performance of the program in the order of 20 times.

The objective of the program is to calculate that bacteria possess the longest possible DNA chain and the shortest doubling time (creation of a new bacterium), opposing objectives.

Bacteria perform tasks according to what they have encoded in their DNA chain.

A DNA chain as long as possible allows you to create bacteria programmed to perform a greater amount of tasks.

A shorter doubling time means that the population of bacteria grows faster. Therefore more bacteria develop the same task so the global task is done faster. For example, consume oil from a loss in the ocean.

In conclusion, finding the bacteria with the largest DNA chains and the shortest doubling time is finding the most flexible bacteria that perform the most rapid tasks.
