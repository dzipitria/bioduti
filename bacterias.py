# -*- coding: utf-8 -*-
#   Este programa es software libre y se publica bajo GNU Lesser General Public v3
#
#   Para la realización de este proyecto se usaron las librerias: DEAP, NUMPY, JSON que al igual que
#   Python son software libre GNU
#   Puede ver la licencia GNU en: http://www.gnu.org/licenses/
#   
#   Daniel Zipitría
#
#   Este modulo carga las bacterias  del archivo datos.csv  y las deja cargadas en la clase: bact

from funciones import Distancia
from funciones import estimador_rbf
from random import sample
from random import uniform
from math import exp

class bacterias():
    def __init__(self, archivo='datos.csv'):
        self.ValMax = [80.0, 1.0, 15000000.0, 12.0, 0.5, 11000.0]
        self.ValMin = [20.0, 0.2, 400000.0, 0.0, 0.15, 519.0]
        self.ValNorMax = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]  # Valor normal maximo
        self.ValNorMin = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # Valor normal minimo
        self.bacterias = None
        self.tiempos = None
        self.bacterias_norm = None
        self.bacteria_generador = None  #generador de bacterias automatrico
        self.cargo_bacterias(archivo)
        self.Norm_bacterias()
        self.EstimadorRbf = estimador_rbf(self.bacterias, self.tiempos, 'inverse')

    def cargo_bacterias(self, archivo):
        archivo = open(archivo,'r')
        bacterias = []
        tiempos = []
        for linea in archivo:
            datos = linea.split("\t")
            bacteria=[None] * 6
            bacteria[0] = float(datos[3])
            bacteria[1] = float(datos[4])
            bacteria[2] = float(datos[5])
            bacteria[3] = float(datos[6])
            bacteria[4] = float(datos[7])
            aux = datos[8].split("\n")
            bacteria[5] = float(aux[0])
            bacterias.append(bacteria)
            tiempos.append(float(datos[2]))
        self.bacterias = bacterias
        self.tiempos = tiempos


    def Norm(self, ind):
        """Dados un individuo y los limites normaliza los valores de cromosomas del individuo """
        IndNorm = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in range(len(ind)):
            #print"valor ind " + str(ind[i])
            aux = float(self.ValMax[i]) - float(self.ValMin[i])
            #print "aca"
            IndNorm[i] = (ind[i] - self.ValMin[i]) / (aux) * (self.ValNorMax[i] - self.ValNorMin[i]) + self.ValNorMin[i]
        return IndNorm

    def DesNorm(self, IndNorm):
        """Dados un individuo normalizado y los limites normaliza los valores de cromosomas del individuo """
        ind = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in range(len(ind)):
            ind[i] = (IndNorm[i] - self.ValNorMin[i]) / (self.ValNorMax[i] - self.ValNorMin[i]) * (self.ValMax[i] - self.ValMin[i]) + self.ValMin[i]
        return ind

    def Norm_bacterias(self):
    #   Norm_bacterias: Normaliza los datos de las bacterias
        bac_norm = []
        for b in self.bacterias:
            bac_norm.append(self.Norm(b))
        self.bacterias_norm = bac_norm

    def BactSercanas(self, bact, k = 3):
    #  Devuelve los indices de las k bacterias más cercanas
        if k > len(self.bacterias_norm):
            return []
        menores_dist=[None] * k
        max_min = 0  # El maximo de los minimos valores
        for i in range(len(self.bacterias_norm)):
            if i < k:
                d = Distancia(self.bacterias_norm[i], bact)
                menores_dist[i] = [d, i]
                if max_min < d:
                    max_min = d
            else:
                d = Distancia(self.bacterias_norm[i], bact)
                if d < max_min:
                    d_aux = d
                    i_aux = i
                    max_min = 0
                    for j in range(k):
                        if menores_dist[j][0] > d_aux:
                            aux_dist = menores_dist[j][0]
                            aux_indice = menores_dist[j][1]
                            menores_dist[j][0] = d_aux
                            menores_dist[j][1] = i_aux
                            d_aux = aux_dist
                            i_aux = aux_indice
                        if menores_dist[j][0] > max_min:
                            max_min = menores_dist[j][0]
        return menores_dist

    def Verifico_bacteria(self, bact):
    #   Retorna falso si algun gen de la bacteria no esta en rango, sino retorna true
        for i in range(len(bact)):
            if not((bact[i] <= self.ValMax[i]) and (bact[i] >= self.ValMin[i])):
                print ("\tgen: %d: %s, %s, %s" % (i, str(self.ValMin[i]), str(bact[i]), str(self.ValMax[i])))
                return False
        return True

    def FitnessDist(self, bact):
        """  Parametros:
                bact: Bacteria a evaluar normalizada
                bacts: bacterias para comparar
        """
        bs = self.BactSercanas(bact)
        tiempos_pond = 0
        inv_dist = 0
        for b in bs:
            #print ("Distancia: %f, IndiceBact: %i, Tiempo: %f") % (b[0], b[1], tiempos[b[1]])
            if b[0] == 0:
                return self.tiempos[b[1]], bact[2]
            tiempos_pond += (self.tiempos[b[1]]) / (b[0])
            inv_dist += 1.0 / (b[0])
        return round(tiempos_pond / inv_dist,4) , bact[2]

    def FitnessRbf(self, BactNorm):
        b = self.DesNorm(BactNorm)
        estimador = round(1.0 / self.EstimadorRbf(b[0], b[1], b[2], b[3], b[4], b[5]), 1)
        return estimador, BactNorm[2]

    def FitnessDistRbf(self, BactNorm):
        """
        Fitnes ponderadndo entre Rbd y  Ditancia, usa la fincuion phi (interal de la Normal(0,1))
        sinusoide para generar una trancicion entre FitnesDist y FitnesRbf.
        centro: es el punto de trancicion entre una funcion y otra.
        vertical: pendiente de trancicion.
        """
        centro = 0.10
        vertical = 90.0
        # Calculo estimador usando Distancia de 3 puntos
        bs = self.BactSercanas(BactNorm)
        tiempo_dist = 0.0
        inv_dist = 0.0
        prom_dist = 0.0
        for b in bs:
            if b[0] == 0:
                min_dist = 0
                prom_dist = 0
                break
            prom_dist += b[0]
            tiempo_dist += (self.tiempos[b[1]]) / (b[0])
            inv_dist += 1.0 / (b[0])
        if prom_dist == 0:
            estimador_dist = self.tiempos[b[1]]
        else:
            prom_dist = prom_dist / len(bs)
            estimador_dist = tiempo_dist / inv_dist
        # Calculo estimador Rbf
        b = self.DesNorm(BactNorm)
        estimador_rbf = 1.0 / self.EstimadorRbf(b[0], b[1], b[2], b[3], b[4], b[5])
        # Pondero los estimadores
        # k = 1.0 / (1 + exp(-vertical * (min_dist - centro)))
        k = 1.0 / (1.0 + exp(-vertical * (prom_dist - centro)))
        estimador = estimador_dist * (1 - k) + estimador_rbf * k
        if estimador < 1.5:  # Para aumentar el muestreo cuando el tiempo es menor a 1.5 horas afecta a bioduti funcion aprox
            estimador = round(estimador, 2)
        else:
            estimador = round(estimador, 1)
        return estimador, BactNorm[2]

    def genero_bacterias(self, isla=0, cant_islas=1, cant_ind_isla=10):
        """Crea un generador que sirve para contruir una  población de individuos distribuida por el espcio
            de acuerdo a los parametros:
                cant_islas: Tipo entero la, cantidad de islas  se usa para distribuir los cromosomas en grupos
                cant_ind_islas: Tipo entero, cantidad de individuios por isla se usa para que el conjunto de
                total de individuos tenga cromosomas en cada zona del espacio.
            Crea un iterador de individuos, cada individuo es devuelto a invocar la función:
                self.genero_bacteria
        """
        # se generan secuencias de numeros aleatorios para logra obtener las muestras de forma aleatoria
        # pero cubriendo todos los espacios posibles
        cuadrante = []  # Cuadrantes para 5 de las 6 variables
        for n in range(5):
            cuadrante.append(sample(range(cant_ind_isla), cant_ind_isla))
        #for ci in range(cant_islas):  # para cada isla
        individuo = [None] * 6
        for i in range(cant_ind_isla):  # para cada individuo en la isla
            for n in range(6):  # para cada cromosoma en el individuo
                if n < 2:
                    intervalo_cromosoma = (self.ValNorMax[n] - self.ValNorMin[n]) / cant_ind_isla
                    cromo_min = cuadrante[n][i] * intervalo_cromosoma  + self.ValNorMin[n]
                    cromo_max = (cuadrante[n][i] + 1) * intervalo_cromosoma  + self.ValNorMin[n]
                    individuo[n] = uniform(cromo_min, cromo_max)
                elif n == 2:
                    intervalo_cromosoma = (self.ValNorMax[2] - self.ValNorMin[2]) / cant_islas
                    cromo_min = isla * intervalo_cromosoma  + self.ValNorMin[2]
                    cromo_max = (isla + 1) * intervalo_cromosoma  + self.ValNorMin[2]
                    individuo[2] = uniform(cromo_min, cromo_max)
                else:
                    intervalo_cromosoma = (self.ValNorMax[n] - self.ValNorMin[n]) / cant_ind_isla
                    cromo_min = cuadrante[n - 1][i] * intervalo_cromosoma  + self.ValNorMin[n]
                    cromo_max = (cuadrante[n - 1][i] + 1) * intervalo_cromosoma  + self.ValNorMin[n]
                    individuo[n] = uniform(cromo_min, cromo_max)
            yield individuo

    def genero_bacteria(self, isla=0, cant_islas=1, cant_ind_isla=10):
        if self.bacteria_generador == None:
            self.bacteria_generador = self.genero_bacterias(isla, cant_islas, cant_ind_isla)
            return self.bacteria_generador.next()
        else:
            return self.bacteria_generador.next()


#Creo bact: Un conjunto de bacterias ya normalizadas prontas para usar
bacts = bacterias()

if __name__ == "__main__":
    print bacts.genero_bacteria(0,1,10)
    print bacts.genero_bacteria()
    print bacts.genero_bacteria()
    print bacts.genero_bacteria()
    print "----------------"
    for ind in bacts.genero_bacterias(0,1,3):
        print ind
    print "        "
    for ind in bacts.genero_bacterias(3,4,3):
        print ind
    print ".....::::......"
    b = bacterias("datos_test.csv")
    # b = bacterias()
    b1 = bacts.bacterias_norm[0]
    print bacts.tiempos[0]
    print b.FitnessRbf(b1)
    print b.FitnessDist(b1)
    print b.FitnessDistRbf(b1)
    print b.BactSercanas(b1)
    print("----------------")
    b1 = bacts.bacterias_norm[1]
    print bacts.tiempos[1]
    print b.FitnessRbf(b1)
    print b.FitnessDist(b1)
    print b.FitnessDistRbf(b1)
    print b.BactSercanas(b1)
    print("----------------")
    b1 = bacts.bacterias_norm[24]
    print bacts.tiempos[24]
    print b.FitnessRbf(b1)
    print b.FitnessDist(b1)
    print b.FitnessDistRbf(b1)
    print b.BactSercanas(b1)
    print("----------------")
    b1 = bacts.bacterias_norm[208]
    print bacts.tiempos[208]
    print b.FitnessRbf(b1)
    print b.FitnessDist(b1)
    print b.FitnessDistRbf(b1)
    print b.BactSercanas(b1)
    print("----------------")
    b1 = bacts.bacterias_norm[209]
    print bacts.tiempos[209]
    print b.FitnessRbf(b1)
    print b.FitnessDist(b1)
    print b.FitnessDistRbf(b1)
    print b.BactSercanas(b1)
